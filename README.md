# README #

Testülesanne arendajale
Ülesanne on jagatud 4 erinevaks etapiks. 1 etapp on kõige lihtsam ja 4 etapp kõige keerulisem. Ei ole
kohustuslik lahendada kõiki etappe. Hakka otsast pihta ning tee omale jõukohased etapid ära. Võid
ka liikuda kohe viimasesse etappi või omavahel segada etappide töid.
Lahendamise ajal pane kirja tööks kulunud aeg ning tee märkmeid sellekohta, et mis oli lihtne, mis oli
kerge ning kui jäid mõne probleemi lahendamise puhul jänni, siis kust said abi ja kuidas probleemi
lahendasid. Väga oluline on see, et sa dokumenteeriks oma lahenduse. Meie poolt hindab ja käivitab
lahendust inimene, kes pole varem seda arendusprojekti näinud. Tal peab olema väga lihtne
rakendust tööle panna ning paha ei teeks dokument, mis annab ülevaate sellest, mida tegid ja kuidas
tegid.
Ülesande kirjeldus:
Hambaarsti juurde registreerimise rakendus - eesmärk on kasutajale pakkuda mugavat kasutajaliidest
registreerimaks hambaarsti vastuvõtule. Ülesandega on kaasa pandud tehniline projekti põhi, mis on
üles ehitatud kasutades Maven ja Spring raamistikke. Sinu ülesanne on sinna teha vajalikud
täiendused vastavalt etappide kirjeldusele.
1) ETAPP 1 - arendusprojekti seadistamine ning esmased muudatused
1.1) impordi oma arendusvahendisse (Eclipse või IntelliJ Idea) Maveni projekt ja käivita rakendus.
Rakendus baseerub Spring boot raamistikul ning käivitamisel ilmub logisse ka rakenduse
käivitamiseks vajalik URL.
1.2) Tee esmased muudatused kasutajaliidesesse - Hetkel avanevas kasutajaliideses on hambaarsti
vastuvõtule registreerimise vorm. Lisa sinna kaks välja: valiklahter, kust saab valida perearsti nime
ning samuti soovitava visiidi kuupäeva ja kellaaja valik. Lisa lisatud lahtritele lisaks kasutajaliidese
poolsetele validatsioonidele serveri poolsed valideeringud ning salvestamine andmebaasi (mälus
töötav ja juba liidestatud andmebaas)

2) ETAPP 2 - Registreeringute vaatamise vaade
2.1) Lisa rakendusele funktsionaalsus registreeringute vaatamiseks. Tekita menüüsse uus
menüüpunkt ning sellele vajutades kuva kasutajale kõik süsteemi lisatud registreeringud.

3) ETAPP 3 - Otsing
3.1) Muuda registreeringute vaate vormi selliselt, et tekiks otsingu funktsionaalsus ning otsingu
tulemustele vajutades avaneks detailne registreeringu vaate vorm.
4) ETAPP 4 - Funktsionaalsuse täiendamine
4.1) Lisa registreeringute detail vaatele võimalus registreeringuid kustutada
4.2) Lisa registreeringute detail vaatele võimalus registreeringuid muuta
4.3) Lisa registreeringu esitamise vaatele funktsionaalsus broneeringu aegade kontrolliks - kui
kasutaja üritab broneerida hambaarsti vastuvõtu aega, mis on juba kellegi teise poolt võetud või
osaliselt kattuv, siis lisa vastav veateade ja keela broneeringu edastamine.
Tagasisaatmise juhend:
Ülesande lahendus commit’i mõnda vabalt kättesaadavasse versioonihaldus süsteemi - näiteks
BitBucket või mõni muu alternatiiv. Juhul kui sul on oma veebiserver või dropbox, võid ka koodi
kokku pakkida ja saata meile allatõmbamiseks näiteks ZIPi lingi – sinu vaba valik. Veendu, et link oleks
meile kättesaadav.
Link lahendusele peab olema tagasi saadetud hiljemalt 17.04. kell 23.59. meiliaadressile
mare.palvari@cgi.com
Igal juhul veendu, et sa oled oma projektiga kaasa pannud kõik failid projekti käivitamiseks (arvesta,
et projekti käivitab inimene, kes pole varem sinu koodi näinud. Tee juhend ning kõik puust punaseks
ette).
Probleemid
Probleemide korral võta meie ühendust. Aitame hea meelega ning vajadusel jagame lisa infot. 